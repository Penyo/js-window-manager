﻿/**
 * Make ajax request.
 * @param {string} url
 * @param {function} successFunction
 * @param {function} errorFunction
 * @param {string} type
 * @return {void}
*/
function ajaxRequest(url, successFunction, errorFunction, type) {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function ajaxReadyState() {
        if (xmlhttp.readyState === XMLHttpRequest.DONE) {
            if (xmlhttp.status === 200) {
                successFunction(xmlhttp.response);
            }
            else {
                errorFunction(xmlhttp.response);
            }
        }
    };
    xmlhttp.open(type, url, true);
    xmlhttp.send();
}

/**
 * get element(s) from input selector.
 * @param {string} selector
 * @param {bool} firstOnly
 * @param {Element} scope
 * @return {Element or Collection}
*/
function get(selector, firstOnly, scope) {
    var regexCollection = {
        id: /^#([a-zA-Z0-9-_]+)$/,      /*/^#(\w+$)/,*/
        cl: /^#([a-zA-Z0-9-_]+)$/       /*/^\.(\w+$)/*/
    },
        scope = scope || document;
    if (regexCollection.id.test(selector)) {
        var idSel = selector.match(regexCollection.id)[1];
        return scope.getElementById(idSel);
    }
    if (regexCollection.cl.test(selector)) {
        var classSel = selector.match(regexCollection.cl)[1],
            results = scope.getElementsByClassName(classSel);
        return firstOnly ? results[0] : results;
    }
    return (firstOnly ? scope.querySelector.bind(scope) : scope.querySelectorAll.bind(scope))(selector);
}

/**
 * Check input variable is <NULL> or <UNDEFINED>.
 * @param {variable} obj
 * @return {bool}
*/
function nullOrUndefined(obj) {
    return obj === null || obj === undefined;
}

/**
 * Get current date and time in custom format.
 * @return {string}
*/
function getDateAndTime() {
    var currentDate = new Date(),
        days = currentDate.getDate(),
        months = currentDate.getMonth() + 1,
        hours = currentDate.getHours(),
        minutes = currentDate.getMinutes(),
        seconds = currentDate.getSeconds();
    return (days < 10 ? "0" : "") + days + "." + (months < 10 ? "0" : "") + months + "." + currentDate.getFullYear() + " " +
    (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
}