/**
 * Controller goes Here
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {

    /**
     * Successful response from Ajax request.
     * @param {string} response.
     * @return {Void}
     */
    function _loadTemplateSuccess(response) {
        controller.templates.uiWindow = response;
    }

    /**
     * Error response from Ajax request.
     * @param {string} response.
     * @return {Void}
     */
    function _loadTemplateError(response) {
        console.error(response);
        alert("Can't load UI Window Template");
    }

    GLOBAL.controller = GLOBAL.controller || {};

    controller.UIWindowsArray = {};
    controller.currentActiveUIWindow = null;
    controller.elements = {};
    controller.templates = {};

    /*
     * Add new UI Window and make it visible.
     * @return {Void}
     */
    controller.addWindow = function () {
        var objID = "uiw_" + (new Date()).getTime(),
            uiWindow = new UIWindow(objID),
            icon = new Icon(objID);
        this.UIWindowsArray[objID] = {
            uiWindow: uiWindow,
            icon: icon
        };
        uiWindow.render();
        icon.render();
        this.elements.uiWindowsContainer.appendChild(uiWindow.DomObject);
        this.elements.taskbarIcons.appendChild(icon.DomObject);
        this.switchWindow(objID);
    };

    /*
     * Destroy UI Window event.
     * @param {String|Number} windowID
     * @return {Void}
     */
    controller.destroyWindow = function (windowID) {
        var uiObj = this.UIWindowsArray[windowID];
        //uiObj.icon.destroy();
        Events.publish(uiObj.icon, "destroy");
        if (uiObj == this.currentActiveUIWindow) {
            this.currentActiveUIWindow = null;
        }
        delete this.UIWindowsArray[windowID];
    };

    /*
     * Ajax request to load the templates.
     * @return {Void}
     */
    controller.preloadResources = function () {
        ajaxRequest("templates/uiWindowTemplate.html", _loadTemplateSuccess, _loadTemplateError, "GET");
    };

    /*
     * Get Application's main elements.
     * @return {Void}
     */
    controller.getElements = function () {
        this.elements.btnStart = get("#btnStart");
        this.elements.taskbarIcons = get("#taskbarIcons");
        this.elements.uiWindowsContainer = get("#uiWindowsContainer");
    };

    /*
     * Attach events to Application's main elements.
     * @return {Void}
     */
    controller.attachEvents = function () {
        Events.subscribe(this.elements.btnStart, "click", this.addWindow.bind(this));
        Events.subscribe(this, "destroy-window", this.destroyWindow.bind(this));
        Events.subscribe(this, "minimize-window", this.minimizeWindow.bind(this));
        Events.subscribe(this, "switch-window", this.switchWindow.bind(this));
    };

    /*
     * Switch active UI Window. If current and new "active" windows are the same - just minimize it.
     * @param {String|Number} uID
     * @return {Void}
     */
    controller.switchWindow = function (uID) {
        var prevAndNewAreSame = false;
        if (this.currentActiveUIWindow != null) {
            prevAndNewAreSame = this.currentActiveUIWindow.uiWindow.id === uID;
            Events.publish(this.currentActiveUIWindow.uiWindow, "window-minimize");
        }
        if (prevAndNewAreSame == false) {
            var newActive = this.UIWindowsArray[uID];
            if (nullOrUndefined(newActive)) {
                throw ("Can't find window with ID:" + uID);
            }
            this.currentActiveUIWindow = newActive;
            Events.publish(newActive.uiWindow, "window-maximize");
            Events.publish(newActive.icon, "set-icon-active");
        }
    };

    /*
     * Minimize UI Window event.
     * @param {String|Number} uID
     * @return {Void}
     */
    controller.minimizeWindow = function (uID) {
        var uiWinObject = this.UIWindowsArray[uID];
        this.currentActiveUIWindow = null;
        Events.publish(uiWinObject.icon, "set-icon-inactive");
    };

    /*
     * Controller's Initialize function.
     * @return {Void}
     */
    controller.init = function () {
        this.preloadResources();
        this.getElements();
        this.attachEvents();
    };

    controller.init();

})(window);