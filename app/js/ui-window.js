/**
 * Window goes Here
 * @param  {Object} GLOBAL
 * @return {Void}
 */
(function (GLOBAL) {

    /*
     * UIWindow class
     * @param {String|Number} uiWindowID
     * @return {Void}
     */
    function UIWindow(uiWindowID) {
        this.id = uiWindowID;
        this.elements = {};
        this.DomObject = null;
        this.Draggable = null;
    }    

    /*
     * Get UIWindow's elements.
     * @return {Void}
     */
    UIWindow.prototype.getElements = function () {
        this.elements.BtnMinimize = get(".btn-minimize", true, this.DomObject);
        this.elements.BtnResize = get(".btn-resize", true, this.DomObject);
        this.elements.BtnClose = get(".btn-close", true, this.DomObject);
        this.elements.SystemInfo = get(".system-info", true, this.DomObject);
        this.elements.ActionsInfo = get(".actions-info", true, this.DomObject);
        this.elements.WindowStatusbar = get(".ui-window-statusbar", true, this.DomObject);
        this.elements.WindowTitlebar = get(".ui-window-titlebar", true, this.DomObject);
    };

    /*
     * Attach events to UIWindow and UIWindow's elements.
     * @return {Void}
     */
    UIWindow.prototype.attachEvents = function () {
        this.events = {
            resize: this.resize.bind(this),
            minimize: this.minimize.bind(this),
            destroy: this.destroy.bind(this),
            maximize: this.maximize.bind(this),
            changeClass: this.setStatusbarContent.bind(this),
            logAction: this.logAction.bind(this)
        };
        Events.subscribe(this.elements.BtnResize, "click", this.events.resize);
        Events.subscribe(this.elements.BtnMinimize, "click", this.events.minimize);
        Events.subscribe(this.elements.BtnClose, "click", this.events.destroy);
        Events.subscribe(this, "window-maximize", this.events.maximize);
        Events.subscribe(this, "change-class", this.events.changeClass);
        Events.subscribe(this, "log-action", this.events.logAction);
        Events.subscribe(this, "window-minimize", this.events.minimize);
    };

    /*
     * Dettach events from UIWindow and UIWindow's elements.
     * @return {Void}
     */
    UIWindow.prototype.dettachEvents = function () {
        Events.unsubscribe(this.elements.BtnResize, "click", this.events.resize);
        Events.unsubscribe(this.elements.BtnMinimize, "click", this.events.minimize);
        Events.unsubscribe(this.elements.BtnClose, "click", this.events.destroy);
        Events.unsubscribe(this, "window-maximize", this.events.maximize);
        Events.unsubscribe(this, "change-class", this.events.changeClass);
        Events.unsubscribe(this, "log-action", this.events.logAction);
        Events.unsubscribe(this, "window-minimize", this.events.minimize);
    };

    /*
     * UIWindow's initialize function.
     * @return {Void}
     */
    UIWindow.prototype.render = function () {
        var obj = document.createElement("div");
        obj.innerHTML = controller.templates.uiWindow.trim();
        this.DomObject = obj.childNodes[0];
        this.DomObject.dataset["uid"] = this.id;
        var currentWin = this;
        setTimeout(function () {
            currentWin.DomObject.classList.remove("preload");
            Events.publish(currentWin, "change-class");
        }, 50);
        this.getElements();
        this.attachEvents();
        this.setWindowContent();
        this.Draggable = new Draggable(this.DomObject, this.elements.WindowTitlebar, {
            onmousedown: this.removeAnimations.bind(this),
            onmouseup: this.addAnimations.bind(this)
        });
        this.Draggable.init();
    };

    /*
     * Set UIWindow's information to "SystemInfo" element.
     * @return {Void}
     */
    UIWindow.prototype.setWindowContent = function () {
        this.elements.SystemInfo.innerHTML = "ID = " + this.id + "<br />Created on: " + getDateAndTime();
        this.setStatusbarContent();
    };

    /*
     * Set UIWindow's CSS Classes to UIWindow's statusbar.
     * @return {Void}
     */
    UIWindow.prototype.setStatusbarContent = function () {
        this.elements.WindowStatusbar.innerHTML = this.DomObject.className;
    };

    /*
     * Store data for input "action" in the "ActionsInfo" element.
     * @param {String} actionName
     * @return {Void}
     */
    UIWindow.prototype.logAction = function (actionName) {
        this.elements.ActionsInfo.innerHTML += getDateAndTime() + ": <b>" + actionName + "</b><br />";
    };

    /*
     * Destroy UIWindow event.
     * @param {Event} e
     * @return {Void}
     */
    UIWindow.prototype.destroy = function (e) {
        if (e && typeof e.preventDefault == "function") {
            e.preventDefault();
        }
        this.dettachEvents();
        this.DomObject.remove();
        this.Draggable.destroy();
        Events.publish(controller, "destroy-window", this.id);
    };

    /*
     * Minimize UIWindow event.
     * @param {Event} e
     * @return {Void}
     */
    UIWindow.prototype.minimize = function (e) {
        if (e && typeof e.preventDefault == "function") {
            e.preventDefault();
        }
        this.DomObject.classList.add("minimized");
        Events.publish(this, "change-class");
        Events.publish(controller, "minimize-window", this.id);
        Events.publish(this, "log-action", "minimize");
    };

    /*
     * Maximize UIWindow event.
     * @return {Void}
     */
    UIWindow.prototype.maximize = function () {
        this.DomObject.classList.remove("minimized");
        Events.publish(this, "change-class");
        Events.publish(this, "log-action", "maximize");
    };

    /*
     * Resize UIWindow event.
     * @param {Event} e
     * @return {Void}
     */
    UIWindow.prototype.resize = function (e) {
        if (e && typeof e.preventDefault == "function") {
            e.preventDefault();
        }
        var added = this.DomObject.classList.toggle("full-screen");
        Events.publish(this, "change-class");
        Events.publish(this, "log-action", "resize - " + (added ? "full-screen" : "small-screen"));
    };

    /*
     * Remove CSS animations from the UIWindow's container.
     * @return {Void}
     */
    UIWindow.prototype.removeAnimations = function () {
        this.DomObject.classList.add("no-animations");
        Events.publish(this, "log-action", "mouse down");
    }

    /*
     * Add CSS animations to the UIWindow's container.
     * @return {Void}
     */
    UIWindow.prototype.addAnimations = function () {
        this.DomObject.classList.remove("no-animations");
        Events.publish(this, "log-action", "mouse up");
    }

    GLOBAL.UIWindow = UIWindow;
})(window);