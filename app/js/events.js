/***
* Subsciing and Publishing application events
* - Extend to can pass data with the events
* - Optional: trigger events on object
*/
var Events = (function () {
    var customEvents = [];
    /***
     * Create event
     * @param {String} eventName
     * @param {Object} data
     * returns {Event Object}
     */
    function create(eventName, data) {
        var event = new CustomEvent(eventName, data);

        return event;
    };
    return {
        /***
         * Subscribes a listener to event and trigger a handler
         * @param {Dom Object} el
         * @param {String} eventName
         * @param {Function} handler
         * returns void
         */
        subscribe: function (el, eventName, handler) {
            if (el.addEventListener) { //DOM element
                el.addEventListener(eventName, handler, false);
            }
            else {
                customEvents.push({
                    object: el,
                    event: eventName,
                    handler: handler
                });
            }
        },

        /***
         * Publishes an event to the listneres
         * @param {Dom Object} el
         * @param {String} eventName
         * @param {Object} data
         * returns void
         */
        publish: function (el, eventName, data) {
            if (el.dispatchEvent) { // DOM element
                var event = create(eventName, data);
                el.dispatchEvent(event);
            }
            else {
                var allCustomEvents = customEvents.filter(function (item) {
                    return item.object === el && item.event === eventName;
                });
                for (var i = 0 ; i < allCustomEvents.length; i++) {
                    allCustomEvents[i].handler.call(el, data);
                }
            }
        },

        /***
         * Removes a listener from element and detach handler function
         * @param {Dom Object} el
         * @param {String} eventName
         * @param {Function} handler
         * returns void
         */
        unsubscribe: function (el, eventName, handler) {
            if (el.removeEventListener) { //DOM element
                el.removeEventListener(eventName, handler, false);
            }
            else {
                var index = customEvents.findIndex(function (item) {
                    return item.object === el && item.event == eventName && item.handler === handler;
                });
                if (index > -1) {
                    customEvents.splice(index, 1);
                }
            }
        }
    };
})();