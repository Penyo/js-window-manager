﻿(function (GLOBAL) {
    var _activeIconClass = "active-icon";
    
    /*
     * Icon class
     * @param {String|Number} uiWindowID
     * @return {Void}
     */
    function Icon(uiWindowID) {
        this.id = uiWindowID;
        this.DomObject = null;
    }

    /*
     * Icon's initialize function.
     * @return {Void}
     */
    Icon.prototype.render = function () {
        this.DomObject = document.createElement("button");
        this.DomObject.className = "clean-btn fa fa-2x fa-external-link";
        this.DomObject.dataset["uid"] = this.id;
        this.attachEvents();
    };

    /*
     * Toggle icon's active CSS class.
     * @param {Boolean} toBeActive
     * @return {Void}
     */
    Icon.prototype.toggleActive = function (toBeActive) {
        if (toBeActive) {
            this.DomObject.classList.add(_activeIconClass);
        }
        else {
            this.DomObject.classList.remove(_activeIconClass);
        }
    };

    /*
     * Set Icon's active CSS class.
     * @return {Void}
     */
    Icon.prototype.setActive = function () {
        this.toggleActive(true);
    };

    /*
     * Remove Icon's active CSS class.
     * @return {Void}
     */
    Icon.prototype.setInactive = function () {
        this.toggleActive(false);
    };

    /*
     * Destroy the current Icon.
     * @return {Void}
     */
    Icon.prototype.destroy = function () {
        this.dettachEvents();
        this.DomObject.remove();
    };

    /*
     * Attach events to the Icon and elements in its DOM representation.
     * @return {Void}
     */
    Icon.prototype.attachEvents = function () {
        this.events = {
            toggleWindow: this.toggleWindow.bind(this),
            destroy: this.destroy.bind(this),
            setActive: this.setActive.bind(this),
            setInactive: this.setInactive.bind(this),
        };
        Events.subscribe(this.DomObject, "click", this.events.toggleWindow);
        Events.subscribe(this, "set-icon-active", this.events.setActive);
        Events.subscribe(this, "set-icon-inactive", this.events.setInactive);
        Events.subscribe(this, "destroy", this.events.destroy);
    };

    /*
     * Dettach events to the Icon and elements in its DOM representation.
     * @return {Void}
     */
    Icon.prototype.dettachEvents = function () {
        Events.unsubscribe(this.DomObject, "click", this.events.toggleWindow);
        Events.unsubscribe(this, "set-icon-active", this.events.setActive);
        Events.unsubscribe(this, "set-icon-inactive", this.events.setInactive);
        Events.unsubscribe(this, "destroy", this.events.destroy);
    };

    /*
     * Call controller's switch-window event.
     * @return {Void}
     */
    Icon.prototype.toggleWindow = function () {
        Events.publish(controller, "switch-window", this.id);
    };

    GLOBAL.Icon = Icon;
})(window);