﻿(function (GLOBAL) {
    var _mouseHold = false;
    var _privateEvents = {
        variables: {},
        onMouseDown: function (elements, e) {
            _mouseHold = true;
            this.move = this.onMove.bind(this, elements);
            this.variables = {
                posX: e.clientX,
                posY: e.clientY,
                elTop: (elements.elementToDrag.style.top.replace("px", "") || 0),
                elLeft: (elements.elementToDrag.style.left.replace("px", "") || 0)
            };
            this.variables.diffX = this.variables.posX - this.variables.elLeft;
            this.variables.diffY = this.variables.posY - this.variables.elTop;
            Events.subscribe(document, "mousemove", this.move);
        },
        onMouseUp: function (elements) {
            _mouseHold = false;
            Events.unsubscribe(document, "mousemove", this.move);
        },
        onMove: function (elements, e) {
            if (_mouseHold == false) {
                return;
            }
            var xPos = e.clientX - this.variables.diffX,
                yPos = e.clientY - this.variables.diffY;
            
            elements.elementToDrag.style.left = xPos + "px";
            elements.elementToDrag.style.top = yPos + "px";
        }
    };

    function Draggable(elementToDrag, draggedElement, customEvents) {
        this.elements = {
            elementToDrag: elementToDrag,
            draggedElement: draggedElement
        };
        this.customEvents = customEvents;
    }
    Draggable.prototype.init = function () {
        this.attachEvents();
    }
    Draggable.prototype.attachEvents = function () {
        this.events = {
            onmousedown: _privateEvents.onMouseDown.bind(_privateEvents, this.elements),
            onmouseup: _privateEvents.onMouseUp.bind(_privateEvents, this.elements),
            custom: {}
        };
        Events.subscribe(this.elements.draggedElement, "mousedown", this.events.onmousedown);
        Events.subscribe(this.elements.draggedElement, "mouseup", this.events.onmouseup);
        if (nullOrUndefined(this.customEvents) == false) {
            if (nullOrUndefined(this.customEvents.onmousedown) == false && typeof this.customEvents.onmousedown == "function") {
                this.events.custom.onmousedown = this.customEvents.onmousedown;
                Events.subscribe(this.elements.draggedElement, "mousedown", this.events.custom.onmousedown);
            } if (nullOrUndefined(this.customEvents.onmouseup) == false && typeof this.customEvents.onmouseup == "function") {
                this.events.custom.onmouseup = this.customEvents.onmouseup;
                Events.subscribe(this.elements.draggedElement, "mouseup", this.events.custom.onmouseup);
            }
        }
    };
    Draggable.prototype.destroy = function () {
        Events.unsubscribe(this.elements.draggedElement, "mousedown", this.events.onmousedown);
        Events.unsubscribe(this.elements.draggedElement, "mouseup", this.events.onmouseup);
        if (nullOrUndefined(this.customEvents) == false) {
            if (nullOrUndefined(this.events.custom.onmousedown) == false) {
                Events.unsubscribe(this.elements.draggedElement, "mousedown", this.events.custom.onmousedown);
            }
            if (nullOrUndefined(this.events.custom.onmouseup) == false) {
                Events.unsubscribe(this.elements.draggedElement, "mouseup", this.events.custom.onmouseup);
            }
        }
    };

    GLOBAL.Draggable = Draggable;
})(window);
