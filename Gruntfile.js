module.exports = function (grunt) {

    grunt.file.defaultEncoding = 'utf-8';

    /**
     * Configuration
     */
    grunt.initConfig({

        /**
         * Less Tasks
         */
        less: {

            options: {
                sourceMap: true
            },

            development: {
                
                files: [{
                    expand: true,
                    cwd: 'app/less',
                    src: ['**/*.less', '!variables.less'],
                    dest: "build/css/",
                    ext: ".css"
                }]
            },

            production: {
                options: {
                    sourceMap: false,
                },

                files: /*{
                    "production/css/styles.css": "app/less/main.less"
                }*/
                [{
                    expand: true,
                    cwd: 'app/less',
                    src: ['**/*.less', '!variables.less'],
                    dest: "production/css/",
                    ext: ".css"
                }]
            }
        },

        copy: {

            development: {

                files: [      
                    // {expand: true, cwd: 'app/js', src: ['**'], dest: 'build/js'},
                    {expand: true, cwd: 'app/templates', src: ['**'], dest: 'build/templates'},
                ]
            },

            production: {

                files: [      
                    { expand: true, cwd: 'app/templates', src: ['**'], dest: 'production/templates' },
                    { expand: true, cwd: 'app/js', src: ['**'], dest: 'production/js' },
                ]
            },

             js: {
            
                 files: [
                     {expand: true, cwd: 'app/js', src: ['**'], dest: 'build/js'},
                 ]
             },

            templates: {
                
                files: [      
                    {expand: true, cwd: 'app/templates', src: ['**'], dest: 'build/templates'},
                ]
            }
        },

        connect: {
            server: {

                options: {
                    port: 8000,
                    hostname: "*",

                    base: {
                        path: 'build',
                        options: {
                            index: 'index.html',
                            maxAge: 300000
                        }
                    }
                }
            }
        },

        clean: {

            development: {
                src: ["build"]
            },

            production: {
                src: ["production"]
            }
        },

        watch: {

            less: {
                options: {
                    livereload: true
                },
                files: ["app/less/**/*.less"],
                tasks: ["less:development"]
            },

            js: {
                options: {
                    livereload: true
                },
                files: ["app/js/**/*.js"],
                tasks: ["copy:js"]
            },

            templates: {
                options: {
                    livereload: true
                },
                files: ["app/templates/**/*.html"],
                tasks: ["copy:templates"]
            },

            html: {
                options: {
                    livereload: true
                },
                files: ["app/index.html"],
                tasks: ["htmlbuild:development"]
            }
        },

        cssmin: {

            production: {
                files: [{
                  expand: true,
                  cwd: 'production/css',
                  src: ['*.css', '!*.min.css'],
                  dest: 'production/css',
                  ext: '.min.css'
                }]
            }
        },

        // uglify: {
        //     options: {
        //         mangle: false
        //     },
        //
        //     production: {
        //
        //         files:  {
        //             'production/js/main.min.js': ['app/js/**/*.js']
        //         }
        //     }
        // },

        //browserify: {

        //    development: {

        //        files: {
        //            'build/js/app.js': ['app/js/main.js']
        //        },

        //        options: {

        //            browserifyOptions: {
        //                debug: true
        //            },
                    
        //            transform: [
        //                ["babelify", {"presets": ["es2015"]}],
        //                ["stringify", {
        //                    appliesTo: { includeExtensions: ['.html'] }
        //                }]
        //            ],

        //            alias: {
        //                'app': './app/js/app.js',
        //                'events': './app/js/events.js',
        //                'template-window': './app/templates/window-template.html'
        //            }
        //        }
        //    },
        //    production: {

        //        files: {
        //            'production/js/main.min.js': ['app/js/main.js']
        //        },

        //        options: {
        //            transform: [
        //                ["babelify", {"presets": ["es2015"]}],
        //                ["stringify", {
        //                    appliesTo: { includeExtensions: ['.html'] }
        //                }]
        //            ]
        //        }
        //    }
        //},

        htmlbuild: {
            production: {
                src: 'app/index.html',
                dest: 'production/',

                options: {
                    beautify: true,
                    // prefix: '//some-cdn',
                    relative: true,

                    data: {
                        // Data to pass to templates 
                        version: "1.0.0"
                    },
                }
                
            },
            development: {
                src: 'app/index.html',
                dest: 'build/',

                options: {
                    beautify: true,
                    // prefix: '//some-cdn',
                    relative: true,

                    data: {
                        // Data to pass to templates 
                        version: "1.0.0"
                    },
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-browserify');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-html-build');

    grunt.registerTask("default", ["clean:development", "htmlbuild:development", "less:development", "copy:development", "copy:js", "connect", "watch"]);
    grunt.registerTask("production", ["clean:production", "htmlbuild:production", "less:production", "cssmin:production", "copy:production"]);
};